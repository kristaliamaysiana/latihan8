package com.binar.binar.service;

import com.binar.binar.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface EmployeeService {

    public Map insert(Employee employee,Long id);// request lempar objek

    public Map update(Employee employee,Long id); //DI objek request

    public Map delete(Long id);// delete by id

    public Map getAll(); // nampilin semua data

    public Map findByName(String name, Integer page, Integer size);

    Page<Employee> findByNameLike(String name, Pageable pageable);

    public Map getAllNative();
}

