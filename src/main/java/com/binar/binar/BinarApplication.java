package com.binar.binar;

import com.binar.binar.controller.fileupload.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties({
		FileStorageProperties.class
})
@SpringBootApplication
public class BinarApplication {

	public static void main(String[] args) {
		SpringApplication.run(BinarApplication.class, args);
	}

}
